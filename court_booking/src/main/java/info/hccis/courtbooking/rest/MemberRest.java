package info.hccis.courtbooking.rest;

import info.hccis.courtbooking.models.MemberEntity;
import info.hccis.courtbooking.repositories.MemberRepository;
import info.hccis.courtbooking.services.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/rest/members")
public class MemberRest {

    private final MemberService _ms;

    @Autowired
    public MemberRest(MemberRepository mr, MemberService ms) {
        _ms = ms;
    }

    @GetMapping("")
    public ArrayList<MemberEntity> GetAll() {
        return _ms.getAllMembers();
    }

    @PostMapping("/add")
    public MemberEntity addMember(@Valid @RequestBody MemberEntity member) {
        return _ms.saveUserMember(member);
    }
}
