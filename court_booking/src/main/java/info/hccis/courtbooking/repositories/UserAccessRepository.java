package info.hccis.courtbooking.repositories;

import info.hccis.courtbooking.models.UserAccessEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Simple repository for database interaction
 *
 * @author Fred Campos
 * @since 2019-01-12
 */
@Repository
public interface UserAccessRepository extends CrudRepository<UserAccessEntity, Integer> {
    UserAccessEntity findByFirstNameAndLastName(String fName, String lName);

    UserAccessEntity findFirstByFirstNameAndLastName(String fName, String lName);

    UserAccessEntity findTopByFirstNameAndLastNameOrderByUserIdDesc(String fName, String lName);
}
