package info.hccis.courtbooking.repositories;

import info.hccis.courtbooking.models.CourtBookingEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface CourtBookingRepository extends CrudRepository<CourtBookingEntity, Integer> {

    ArrayList<CourtBookingEntity> findByMemberIdAndBookingDateAfterAndBookingDateBefore(int id, String start, String end);

    ArrayList<CourtBookingEntity> findByBookingDateAndStartTimeAfter(String date, String time);

    ArrayList<CourtBookingEntity> findByMemberId(int id);

    ArrayList<CourtBookingEntity> findAllByMemberIdAndBookingDateGreaterThanEqual(int id, String date);

    ArrayList<CourtBookingEntity> findAllByCourtNumber(int courtNum);

    ArrayList<CourtBookingEntity> findAllByCourtNumberAndBookingDateEqualsAndStartTimeEquals(int courtNum, String bookingDate, String startTime);

    ArrayList<CourtBookingEntity> findAllByCourtNumberAndBookingDateEqualsAndStartTimeGreaterThanEqual(int courtNum, String bookingDate, String startTime);
}
