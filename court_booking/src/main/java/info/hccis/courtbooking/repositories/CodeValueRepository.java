/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.courtbooking.repositories;

import info.hccis.courtbooking.models.CodeValueEntity;
import org.springframework.data.repository.CrudRepository;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Amro
 */
@Repository
public interface CodeValueRepository extends CrudRepository<CodeValueEntity, Integer> {
    ArrayList<CodeValueEntity> findCodeValueEntitiesByCodeTypeId(int codeTypeId);
}
