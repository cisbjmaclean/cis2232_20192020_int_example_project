package info.hccis.courtbooking.repositories;

import info.hccis.courtbooking.models.MemberEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Simple repository for database interaction
 *
 * @author Fred Campos
 * @since 2019-01-11
 */
@Repository
public interface MemberRepository extends CrudRepository<MemberEntity, Integer> {

    MemberEntity findByUserId(int userId);
}