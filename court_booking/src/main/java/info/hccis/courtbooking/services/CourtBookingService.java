package info.hccis.courtbooking.services;

import info.hccis.courtbooking.models.CourtBookingEntity;
import info.hccis.courtbooking.repositories.CourtBookingRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CourtBookingService {

    CourtBookingRepository _cbr;

    public CourtBookingService(CourtBookingRepository cbr) {
        _cbr = cbr;
    }


    public boolean hasMultipleBookings(CourtBookingEntity booking) {
        ArrayList<CourtBookingEntity> memberBookings = _cbr.findAllByMemberIdAndBookingDateGreaterThanEqual(booking.getMemberId(), booking.getCreatedDate());
        return memberBookings.size() >= 2;
    }

    public boolean hasSameDayBooking(CourtBookingEntity booking) {
        ArrayList<CourtBookingEntity> courts = _cbr.findAllByCourtNumber(booking.getCourtNumber());
        CourtBookingEntity court = courts.stream().filter(c -> c.getBookingDate().equals(booking.getBookingDate()) && c.getStartTime().equals(booking.getStartTime())).findFirst().orElseGet(null);
        return court != null;
    }
}
