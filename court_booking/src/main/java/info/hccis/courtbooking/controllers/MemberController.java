package info.hccis.courtbooking.controllers;

import info.hccis.courtbooking.models.*;
import info.hccis.courtbooking.repositories.CodeValueRepository;
import info.hccis.courtbooking.services.IOService;
import info.hccis.courtbooking.services.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
/**
 * Controller for the member functionality of the site
 * @since 20191212
 * @author Fred Campos
 */
@Controller
@RequestMapping("/members")
public class MemberController {

    private final MemberService _ms;
    private final IOService _ios;
    private final CodeValueRepository _cvr;

    @Autowired
    public MemberController(MemberService ms, IOService ios, CodeValueRepository cvr) {
        _ms = ms;
        _ios = ios;
        _cvr = cvr;
    }
    /**
     * Lists all members
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("")
    public String list(Model model) {
        model.addAttribute("statuses", _cvr.findCodeValueEntitiesByCodeTypeId(3));
        model.addAttribute("members", _ms.createMemberTransient());

        return "members/list";
    }
    /**
     * Page to add new member
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("/add")
    public String add(Model model) {
        MemberEntity member = new MemberEntity();
        model.addAttribute("member", member);
        model.addAttribute("statuses", _cvr.findCodeValueEntitiesByCodeTypeId(3));
        return "members/member";
    }

    /**
     * Page to edit member
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable int id, Model model) {
        MemberEntity member = _ms.createMemberTransient(id);
        if (member != null) {
            model.addAttribute("member", member);
            model.addAttribute("statuses", _cvr.findCodeValueEntitiesByCodeTypeId(3));
            return "members/member";
        }
        return "redirect:/members";
    }

    /**
     * Page to delete member
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable int id) {
        _ms.deleteUserMember(id);
        return "redirect:/members";
    }

    /**
     * Submit method that processes add and edit and any form submission
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("/submit")
    public String submit(Model model, @Valid @ModelAttribute("member") MemberEntity member,
                         BindingResult bindingResult) {
        String startDate = member.getUnFormattedStartDate();
        String endDate = member.getUnFormattedEndDate();

        if (!startDate.isEmpty() && !endDate.isEmpty() && (Integer.parseInt(startDate) > Integer.parseInt(endDate)))
            bindingResult.rejectValue("membershipStartDate", "", "Start date must be before end date");


        if (bindingResult.hasErrors()) {
            model.addAttribute("statuses", _cvr.findCodeValueEntitiesByCodeTypeId(3));

            System.out.println("Validation error");
            for (ObjectError error : bindingResult.getAllErrors()) {
                System.out.println(error.getDefaultMessage());
            }
            return "members/member";
        }
        _ms.saveUserMember(member);

        return "redirect:/members";
    }

    /**
     * Page to allow member to view member bookings
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("/bookings")
    public String showBookingSearch(Model model) {
        model.addAttribute("members", _ms.createMemberTransient());

        return "members/bookingSearch";
    }

    /**
     * Shows the bookings the member selected
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("/booking")
    public String showBooking(Model model, @RequestParam int userId, @RequestParam String start, @RequestParam String end) {
        model.addAttribute("bookings", _ms.findMemberBookings(userId, start, end));
        return "members/bookings";
    }

    /**
     * Export to file functionality
     * @since 20191212
     * @author Fred Campos
     */
    @RequestMapping("/export")
    public String exportMembers(HttpSession session) throws IOException {

        session.setAttribute("title", "Members");

        ArrayList<MemberEntity> members = _ms.getAllMembers();

        _ios.saveToFile(members, "members");

        return "other/export";
    }
}

