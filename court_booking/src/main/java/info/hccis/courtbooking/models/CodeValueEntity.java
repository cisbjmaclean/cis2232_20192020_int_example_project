package info.hccis.courtbooking.models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "codevalue", schema = "cis2232_fitness")
public class CodeValueEntity {
    private int codeTypeId;
    private int codeValueSequence;
    private String englishDescription;
    private String englishDescriptionShort;
    private String frenchDescription;
    private String frenchDescriptionShort;
    private Timestamp createdDateTime;
    private String createdUserId;
    private Timestamp updatedDateTime;
    private String updatedUserId;

    @Basic
    @Column(name = "codeTypeId", nullable = false)
    public int getCodeTypeId() {
        return codeTypeId;
    }

    public void setCodeTypeId(int codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    @Id
    @Column(name = "codeValueSequence", nullable = false)
    public int getCodeValueSequence() {
        return codeValueSequence;
    }

    public void setCodeValueSequence(int codeValueSequence) {
        this.codeValueSequence = codeValueSequence;
    }

    @Basic
    @Column(name = "englishDescription", nullable = false, length = 100)
    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        this.englishDescription = englishDescription;
    }

    @Basic
    @Column(name = "englishDescriptionShort", nullable = false, length = 20)
    public String getEnglishDescriptionShort() {
        return englishDescriptionShort;
    }

    public void setEnglishDescriptionShort(String englishDescriptionShort) {
        this.englishDescriptionShort = englishDescriptionShort;
    }

    @Basic
    @Column(name = "frenchDescription", nullable = true, length = 100)
    public String getFrenchDescription() {
        return frenchDescription;
    }

    public void setFrenchDescription(String frenchDescription) {
        this.frenchDescription = frenchDescription;
    }

    @Basic
    @Column(name = "frenchDescriptionShort", nullable = true, length = 20)
    public String getFrenchDescriptionShort() {
        return frenchDescriptionShort;
    }

    public void setFrenchDescriptionShort(String frenchDescriptionShort) {
        this.frenchDescriptionShort = frenchDescriptionShort;
    }

    @Basic
    @Column(name = "createdDateTime", nullable = true)
    public Timestamp getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Timestamp createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Basic
    @Column(name = "createdUserId", nullable = true, length = 20)
    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    @Basic
    @Column(name = "updatedDateTime", nullable = true)
    public Timestamp getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Timestamp updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @Basic
    @Column(name = "updatedUserId", nullable = true, length = 20)
    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodeValueEntity that = (CodeValueEntity) o;
        return codeTypeId == that.codeTypeId &&
                codeValueSequence == that.codeValueSequence &&
                Objects.equals(englishDescription, that.englishDescription) &&
                Objects.equals(englishDescriptionShort, that.englishDescriptionShort) &&
                Objects.equals(frenchDescription, that.frenchDescription) &&
                Objects.equals(frenchDescriptionShort, that.frenchDescriptionShort) &&
                Objects.equals(createdDateTime, that.createdDateTime) &&
                Objects.equals(createdUserId, that.createdUserId) &&
                Objects.equals(updatedDateTime, that.updatedDateTime) &&
                Objects.equals(updatedUserId, that.updatedUserId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId);
    }
}
