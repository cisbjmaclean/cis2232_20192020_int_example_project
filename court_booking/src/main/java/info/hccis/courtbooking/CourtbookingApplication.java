package info.hccis.courtbooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourtbookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourtbookingApplication.class, args);
	}

}
